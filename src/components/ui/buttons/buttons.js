import BtnPrimary from './primary_button.vue';
import BtnDanger  from './danger_button.vue';
import BtnSuccess from './success_button.vue';

export  {
    BtnPrimary,
    BtnDanger,
    BtnSuccess
};