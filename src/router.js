import Vue from 'vue';
import Router from 'vue-router';
import Upload from '@/views/Upload.vue';
import Preview from '@/views/Preview.vue';

Vue.use(Router);

export default new Router({
    mode  : 'history',
    routes: [
        {
            path     : '/upload',
            name     : 'upload',
            component: Upload
        },
        {
            path     : '/preview',
            name     : 'preview',
            component: Preview
        },
        {
            path    : '*',
            redirect: { name: 'upload' }
        },
        
    ]
});
