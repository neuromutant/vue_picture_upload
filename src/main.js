import Vue    from 'vue';
import App    from './App.vue';
import router from './router';
import store  from './store';

import {Column, ContainerFluid, Container, Row} from './components/ui/grid/grid';
import {BtnPrimary, BtnDanger, BtnSuccess}      from './components/ui/buttons/buttons';

Vue.component('Column',         Column);
Vue.component('ContainerFluid', ContainerFluid);
Vue.component('Container',      Container);
Vue.component('Row',            Row);
Vue.component('BtnPrimary',     BtnPrimary);
Vue.component('BtnDanger',      BtnDanger);
Vue.component('BtnSuccess',     BtnSuccess);

Vue.config.productionTip = false;

new Vue({
    router,
    store,
    render: h => h(App)
}).$mount('#app');
