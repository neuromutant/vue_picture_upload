export default {
    namespaced: true,
    state     : {
        menuItems: [
            {text: 'Загрузка документов',   name: 'upload' },
            {text: 'Предпросмотр',          name: 'preview' },
        ]
    },
    // Можно было бы использовать MapState, но в шаблонах я обычно использую только MapGetters,
    // тк таким образом более понятно что у нас является внутренними переменными стейта, а что мы хотим выносить в шаблоны
    getters: {
        menuItems: ({menuItems}) => menuItems
    }
};