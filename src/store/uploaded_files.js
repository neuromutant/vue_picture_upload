import {docTypes, docStatuses} from './common';
export default {
    namespaced: true,
    state     : {
        files : [],
        hashes: {},
        docTypes,
        docStatuses
    },
    mutations: {
        ADD_UPLOADED_FILES(state, files){
            const copy  = JSON.parse(JSON.stringify(files));
            const final = copy.filter(x=> !state.hashes[x.hash]);
            copy.forEach( ({hash})=> state.hashes[hash] = 1);
            state.files = state.files.concat(final);
        },
        CHANGE_FILE_DOC_TYPE(state, obj){
            const {index, value} = obj;
            state.files[index].docType = value;
        },
        CHANGE_FILE_STATUS(state, obj){
            const {index, value} = obj;
            state.files[index].status = value;
        },
        REMOVE_FILE(state, index){
            state.files.splice(index, 1);
        },
    },
    getters: {
        files                : ({files})=> files,
        fileImageByIndex     : (state) => index => state.files[index].base64,
        fileNameByIndex      : (state) => index => state.files[index].name,
        fileTypeByIndex      : (state) => index => state.files[index].type,
        filedocTypeByIndex   : (state) => index => state.files[index].docType,
        filesdocStatusByIndex: (state) => index => state.files[index].status,
        docTypes             : ({docTypes}) => docTypes,
        docStatuses          : ({docStatuses}) => docStatuses,
    }
};