export const docTypes     = ['Паспорт','ИНН','Права'];
export const docStatuses  = [
    {text: 'Без статуса', type: 'hidden'},
    {text: 'Одобрено',    type: 'badge-success'}, 
    {text: 'Отклонено',   type: 'badge-danger'}
];