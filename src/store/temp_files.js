import swal from 'sweetalert2';
import {docTypes, docStatuses} from './common';
import {post} from '@/lib/axios';
export default {
    namespaced: true,
    state     : {
        files: [],
        docTypes
    },
    mutations: {
        CREATE_FILE(state){
            const newFile = {
                name   : '',
                type   : '',
                size   : '',
                base64 : '',
                status : docStatuses[0],
                docType: 'Паспорт',
            };
            state.files.push(newFile);
        },
        ADD_FILE(state, obj){
            const {index, value} = obj;
            const currentFile    = state.files[index];
            const {hash}         = value;
            const res            = state.files.find(x=> x.hash === hash);
            if(!res){
                const {name, size, base64} = value;
                currentFile.base64 = base64;
                currentFile.name   = name;
                currentFile.size   = size;
                currentFile.hash   = hash;
                return;
            }
            swal({
                type : 'error',
                title: 'Такой файл уже существует',
            });
        },
        REMOVE_FILE(state, index){
            state.files.splice(index, 1);
        },
        CLEAR_FILES(state){
            state.files = [];
        },
        CHANGE_FILE_NAME(state, obj){
            const {index, value} = obj;
            state.files[index].name = value;
        },
        CHANGE_FILE_DOC_TYPE(state, obj){
            const {index, value} = obj;
            state.files[index].docType = value;
        },
        CHANGE_FILE_STATUS(state, obj){
            const {index, value} = obj;
            state.files[index].status = value;
        },
    },
    getters: {
        files             : (state) => state.files,
        anyFileAdded      : (state) => state.files.find(file=> file.base64),
        fileByIndex       : (state) => index => state.files[index],
        fileImageByIndex  : (state) => index => state.files[index].base64,
        fileNameByIndex   : (state) => index => state.files[index].name,
        fileTypeByIndex   : (state) => index => state.files[index].type,
        filedocTypeByIndex: (state) => index => state.files[index].docType,
        docTypes          : ({docTypes})    => docTypes,
        docStatuses       : ({docStatuses}) => docStatuses,
    },
    actions: {
        uploadFiles({getters, commit}){
            const {files} = getters;
            let formData = new FormData();

            files.forEach( (file, index)=> {
                formData.append("doc"+index, file);     
            });
            post('/upload_image', formData);

            commit('uploadedFiles/ADD_UPLOADED_FILES', files, {root: true}); 
            commit('CLEAR_FILES');
        }
    }
};