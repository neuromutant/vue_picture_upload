import Vue   from 'vue';
import Vuex  from 'vuex';
import menu  from './store/menu';
import tempFiles from './store/temp_files';
import uploadedFiles from './store/uploaded_files';

Vue.use(Vuex);

export default new Vuex.Store({
    modules: {menu, tempFiles, uploadedFiles}
});
