import axios from 'axios';
const image_instance = axios.create({
    baseURL: process.env.URL,
    timeout: 2000,
    headers: { 'Content-Type': 'multipart/form-data' } 
});
const get_instance = axios.create({
    baseURL: process.env.URL,
    timeout: 2000,
});

export const post = async (url, data = {}, log=false) => {
    try {
        const res = await image_instance.post(url, data);
        log && console.log(res);
        return res.data;
    } catch (error) {
        console.error(error);
    }
};
export const get  = async (url) => (await get_instance.get(url)).data;